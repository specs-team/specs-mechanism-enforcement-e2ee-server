/* Crypton Server, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Server.
 *
 * Crypton Server is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Server is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Server.  If not, see <http://www.gnu.org/licenses/>.
*/

describe('Container functionality', function () {
  this.timeout(1500000);
  crypton.auditorUrl = 'http://localhost:5000/attests'
  //crypton.auditorUrl = ''

  var session;
  
  describe('Create account and log in', function () {
    it('Create Bob', function (done) {
      crypton.generateAccount('bob', 'pass', function (err, acct) {
        if (err) throw err;
        done();
      });
    });

    it('Get Bob\'s session', function (done) {
      crypton.authorize('bob', 'pass', function (err, sess) {
        if (err) throw err;
        session = sess;
        done();
      });
    });
  });

  describe('save()', function () {
    it('should save changes', function (done) {
      session.create('fileContainer', function (err, container) {
        assert.equal(err, null);
	container.add('chunks', function() {
	  container.get('chunks', function(err, chunks) {
            for (var i = 0; i < 10000; i++){
              chunks[i] = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
	      //console.log(chunks[i]);
	    }
            container.save(function (err) {
              assert.equal(err, null);
	      done();
            });
	  });
	});
      });
    });
  });

  describe('sync()', function () {
    it('should pull changes into an instantiated container', function (done) {
      // force the session to load from server
      session.containers = [];

      session.load('fileContainer', function (err, container) {
	console.log('----------------------------');
        container.get('chunks', function(err, chunks){
	    console.log(chunks['0']);	
	});
        //console.log(container['chunks'][1]);
        assert.equal(err, null);
        
	done();
      });
    });
  });
});
