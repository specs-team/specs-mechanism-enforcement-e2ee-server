/* Crypton Server, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Server.
 *
 * Crypton Server is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Server is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Server.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

var app = process.app;
var db = app.datastore;
var middleware = require('../lib/middleware');
var verifySession = middleware.verifySession;
var Container = require('../lib/container');

var async = require('async');
var sjcl = require('sjcl-full');
// todo: store the key somewhere safe
var keyHex = 'd2efa36c943809318779cefbbaf1f6cdea064ff32fee74a1a708071eea0924743ed915ef9bd6712410ca65a77891999d';
var keyBN = new sjcl.bn(keyHex);
var curve = sjcl.ecc.curves['c384'];
var signatureKey = new sjcl.ecc.ecdsa.secretKey(curve, keyBN);

/**!
 * ### GET /container/:containerNameHmac
 * Retrieve container records for the given `containerNameHmac`
*/
app.get('/container/:containerNameHmac', verifySession, function (req, res) {
  app.log('debug', 'handling GET /container/:containerNameHmac');

  var accountId = req.session.accountId;
  var containerNameHmac = req.params.containerNameHmac;
  var after = req.query.after || 0;
  var nonce = req.query.nonce;
  app.log('info', nonce);

  var container = new Container();
  container.update('accountId', accountId);

  container.getAfter(containerNameHmac, after, function (err) {
    if (err) {
      console.error(err);
      res.send({
        success: false,
        error: err
      });
      return;
    }
    
    if (app.secProperties !== ''){
	    var attestations = [];
	    // SQL call in postgres/container.js for retrieving records contains 'order by', thus
	    // we can rely that for the fileVersion we simply can use a counter
	    db.getChainHash(containerNameHmac, function (err, chainHash){
		if (err){
		    res.send({
			success: false
		   });
		} else {
		    var chainHash = chainHash.toString();
		    // all attestation will get the last chainHash, that is the only one needed and
		    // it is how it works in CloudProof where only the whole file is retrieved anyway
		    // you can consider the last chainHash as the chainHash of the whole file
		    for (var i = 0; i < container.records.length; i++){
			var r = container.records[i];
			var keyVersion = 0; //todo
			var fileVersion = i;
			var ciphertext = JSON.parse(r.payloadCiphertext).ciphertext
			var fileHash = sjcl.hash.sha256.hash(JSON.stringify(ciphertext));
			fileHash = sjcl.codec.hex.fromBits(fileHash);

			var toBeSigned = containerNameHmac.toString() + keyVersion + fileVersion + fileHash + chainHash;
                        var toBeSignedHash = sjcl.hash.sha256.hash(JSON.stringify(toBeSigned));
			var signature = JSON.stringify(signatureKey.sign(toBeSignedHash, 10));

			var attestation = {containerNameHmac: containerNameHmac,
			    keyVersion: keyVersion, fileVersion: fileVersion, fileHash: fileHash,
			    chainHash: chainHash, nonce: nonce, signature: signature};
			attestations.push(attestation);
		    }
		    res.send({
			success: true,
			records: container.records,
			attestations: attestations
		    });
		}
	    });
    } else {
        res.send({
		success: true,
		records: container.records
	});
    }
  });
});

/**!
 * ### GET /container/:containerNameHmac/:recordVersionIdentifier
 * Retrieve specific record for the given `containerNameHmac` by `recordVersionIdentifier`
*/
app.get('/container/:containerNameHmac/:recordVersionIdentifier', verifySession, function (req, res) {
  app.log('debug', 'handling GET /container/:containerNameHmac/:recordVersionIdentifier');

  var accountId = req.session.accountId;
  var containerName = req.params.containerNameHmac;
  var versionIdentifier = req.params.recordVersionIdentifier;

  var container = new Container();
  container.update('accountId', accountId);
  container.get(containerNameHmac, function (err) {
    if (err) {
      res.send({
        success: false,
        error: err
      });
      return;
    }

    // TODO this has to be a map because records is going to be an array
    if (!container.records[versionIdentifier]) {
      app.log('warn', 'record does not exist');
      res.send({
        success: false,
        error: 'Record identifier does not exist'
      });
      return;
    }

    res.send({
      success: true,
      records: container.records[versionIdentifier] // TODO this won't work
    });
  });
});
