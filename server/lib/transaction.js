/* Crypton Server, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Server.
 *
 * Crypton Server is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Server is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Server.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

var app = require('../app');
var db = app.datastore;
var sjcl = require('sjcl-full');
var Account = require('../lib/account');
var async = require('async');

/**!
 * # Transaction()
 *
 * ````
 * var tx = new Transaction();
 * ````
 */
var Transaction = module.exports = function Transaction () {};

/**!
 * ### create(accountId, callback)
 * Attempt to add a new transaction to the database belonging to the specified `accountId`
 * Adds `transactionId` to transaction object if successful
 * Calls back with error if unsuccessful
 * 
 * @param {Number} accountId
 * @param {Function} callback
 */
Transaction.prototype.create = function (accountId, callback) {
  app.log('debug', 'creating transaction');

  var that = this;
  this.update('accountId', accountId);

  db.createTransaction(accountId, function (err, id) {
    if (err) {
      callback(err);
      return;
    }

    that.update('transactionId', id);
    callback(null);
  });
};

/**!
 * ### get(transactionId, callback)
 * Attempt retreive transaction data from the database for the specified `transactionId`
 * Adds data to transaction object if successful
 * Calls back with error if unsuccessful
 * 
 * @param {Number} transactionId
 * @param {Function} callback
 */
// TODO which data?
Transaction.prototype.get = function (transactionId, callback) {
  app.log('debug', 'getting transaction with id: ' + transactionId);

  var that = this;

  db.getTransaction(transactionId, function (err, transaction) {
    if (err) {
      callback(err);
      return;
    }

    if (!transaction.transactionId) {
      app.log('warn', 'transaction does not exist');
      callback('Transaction does not exist');
      return;
    }

    that.update(transaction);
    callback(null);
  });
};

/**!
 * ### update()
 * Update one or a set of keys in the parent transaction object
 * 
 * @param {String} key
 * @param {Object} value
 *
 * or
 *
 * @param {Object} input
 */
// TODO add field validation and callback
Transaction.prototype.update = function () {
  // update({ key: 'value' });
  if (typeof arguments[0] == 'object') {
    for (var key in arguments[0]) {
      this[key] = arguments[0][key];
    }
  }

  // update('key', 'value')
  else if (typeof arguments[0] == 'string' && typeof arguments[1] != 'undefined') {
    this[arguments[0]] = arguments[1];
  }
};

/**!
 * ### add(data, callback)
 * Add a chunk to current transaction
 * 
 * @param {Object} data
 * @param {Function} callback
 */
Transaction.prototype.add = function (data, callback) {
  app.log('debug', 'adding data to transaction');

  var that = this;

  this.assertOwnership(callback, function () {
    db.updateTransaction(that, data, callback);
  });
};

/**!
 * ### abort(callback)
 * Mark transaction as aborted with database
 * Calls back with error if unsuccessful
 * 
 * @param {Function} callback
 */
Transaction.prototype.abort = function (callback) {
  app.log('debug', 'aborting transaction');

  var that = this;
  this.assertOwnership(callback, function () {
    db.abortTransaction(that.transactionId, callback);
  });
};

/**!
 * ### requestCommit(callback)
 * Request transaction commital with database
 * Calls back with error if request is unsuccessful
 * 
 * @param {Function} callback
 */
Transaction.prototype.requestCommit = function (callback) {
  app.log('debug', 'requesting transaction commit');

  var that = this;
  this.assertOwnership(callback, function () {
    db.requestTransactionCommit(that.transactionId, that.accountId, callback);
  });
};

/**!
 * ### isProcessed(callback)
 * Checks database to see to if transaction has been fully committed
 * Calls back with error if request is unsuccessful
 * otherwise calls back without error, boolean finished status,
 * boolean commit success, and any commit errors
 *
 * @param {Function} callback
 */
Transaction.prototype.isProcessed = function (callback) {
  app.log('debug', 'checking transaction commit status');

  db.transactionIsProcessed(this.transactionId, callback);
};

/**!
 * ### assertOwnership(callback, next)
 * Determine if transaction's `interactingAccount` matches `accountId` from database
 * Calls `next` if successful
 * Calls `callback` with error if unsuccessful
 * 
 * @param {Function} callback
 * @param {Function} next
 */
// TODO consider switching argument order
Transaction.prototype.assertOwnership = function (callback, next) {
  app.log('debug', 'asserting transaction ownership');

  if (this.interactingAccount === this.accountId) {
    next();
  } else {
    app.log('warn', 'transaction does not belong to account');
    callback('Transaction does not belong to account');
  }
};

Transaction.prototype.updateChainHash = function (callback) {
  app.log('debug', 'updating chain hash');
  // todo: check whether this transaction contains addContainerRecord at all
  var that = this;
  db.getAttestation(that.transactionId, function (err, attestation) {
    if (err) {
      callback(err);
      return;
    }

    if (Object.keys(attestation).length == 0) {
      app.log('info', 'attestation does not exist');
      callback(null); // this transaction does not have a corresponding attestation and thus no containerRecord and chainHash is not to be updated, this might be for example when creating account
      return;
    }
    db.getChainHash(attestation.nameHmac.toString(), function(err, chainHash) {
	if (err) {
	    callback(err);
	} else {
	    var h = new sjcl.hash.sha256();
            var hashArrBits = sjcl.codec.hex.toBits(attestation.newHash.toString());
            var chainHashArrBits = [];
            if (chainHash.toString().length > 0){
                chainHashArrBits = sjcl.codec.hex.toBits(chainHash.toString());
            }
            h.update(chainHashArrBits);
            h.update(hashArrBits);
            var newChainHash = sjcl.codec.hex.fromBits(h.finalize());
            app.log('info', chainHash.toString());
            app.log('info', attestation.newHash.toString());
            app.log('info', newChainHash);
            db.updateChainHash(attestation.nameHmac.toString(), newChainHash, function(err){    
                callback(err);
            });
        }
    });

  });

}

Transaction.prototype.isAttestationValid = function (callback) {
  app.log('debug', 'checking attestation');

  var isValid = false;
  var that = this;

  db.getAttestation(that.transactionId, function (err, attestation) {
    if (err) {
      app.log('error', 'retrieving attestation from DB failed');
      callback(false); //todo
      return;
    }

    if (Object.keys(attestation).length == 0) {
      app.log('info', 'attestation does not exist');
      callback(true, false); // this transaction does not have a corresponding attestation and nothing needs to checked, this might be for example when creating account
      return;
    }

    // check fileVersion, keyVersion ... the attestation should correspond to the actual values in the database
    // check fileHash to be really the hash of the payload
    async.waterfall(
	[
	    function(callback) {

    	        db.getContainerRecordsNumber(attestation.nameHmac.toString(), function (err, numberOfRecords) {
		    if(attestation.fileVersion == numberOfRecords){
	                callback(null);
		    } else {
		    	callback('fileVersion problem');
		    }
	        });
	    },
	    function(callback){
    	        db.getContainerRecordCiphertext(that.transactionId, function (err, ct) {
		    var ciphertext = JSON.parse(ct).ciphertext;
	            var hash = sjcl.hash.sha256.hash(JSON.stringify(ciphertext));
		    hash = sjcl.codec.hex.fromBits(hash);
		    if (attestation.newHash.toString() === hash){
		    	callback(null, 'hash ok');
		    } else {
		    	callback("hash problem", 'hash not ok');
		    }
	        });
	    }
	],
	function(err, status) {
	    app.log('info', err);
	    if (err != null){
		callback(false, true);
	    } else {
		var account = new Account();
    		account.getById(that.accountId, function (err) {
        	    if (err) {
	    		app.log('error', err);
			callback(false, true);
        	    } else {
			// todo: use sjcl.ecc.deserialize when available in npm package
	    		var curve = sjcl.ecc.curves[account.signKeyPub.curve];
	    		var point = curve.fromBits(sjcl.codec.hex.toBits(account.signKeyPub.point));
  			var signKeyPub = new sjcl.ecc[account.signKeyPub.type].publicKey(curve, point);
	    
     	    		var toBeSigned = attestation.nameHmac.toString() + attestation.keyVersion + attestation.fileVersion + attestation.newHash;
	    		var toBeSignedHash = sjcl.hash.sha256.hash(JSON.stringify(toBeSigned));
	    		var signature = JSON.parse(attestation.signature.toString())

	    		try {
	        	    var verified = signKeyPub.verify(toBeSignedHash, signature);
  	    		    callback(verified, true);
	    		} catch(err) {
			    app.log('info', err);
			    callback(false, true);
	    		}
		    }
    		});
	    }
	}
    );
  });
};

